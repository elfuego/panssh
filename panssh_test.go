/*
 * Copyright © 2022 Roman Pedchenko
 * Licensed under the MIT License that can be found in the LICENSE file.
 */

package panssh

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"io/fs"
	"io/ioutil"
	"net"
	"path"
	"regexp"
	"strconv"
	"testing"
)

const (
	k2  = "2.key"
	k2p = "2p.key"
	k4  = "4.key"
	k4p = "4p.key"
	pwd = "12345678"
)

func TestConnect(t *testing.T) {
	type results struct {
		usedAgent bool
		wrongConn bool
		host      string
		port      int
		user      string
		authCount int
		cb        bool
	}
	type args struct {
		c *Config
		a agent.Agent
	}
	agentK, agentE := parseKey([]byte(testKey), "")
	okAgent := newAgent(agentK, agentE)
	errAgent := newAgent(nil, errors.New("test"))
	tests := []struct {
		name    string
		args    args
		want    *results
		wantErr assert.ErrorAssertionFunc
	}{
		{"empty",
			args{c: &Config{Port: 22, Pwd: "123", Keys: []K{{kf(k2), ""}}, KeyFiles: []K{{kd(t, k2), ""}}}},
			&results{port: 22, authCount: 0}, assert.NoError,
		},
		{"pwd",
			args{c: &Config{Host: "localhost", Port: 22, User: "root", Pwd: "123", UsePwd: true}},
			&results{host: "localhost", port: 22, user: "root", authCount: 1}, assert.NoError,
		},
		{"key",
			args{c: &Config{Port: 22, Keys: []K{{kd(t, k2), ""}}, UseKeys: true}},
			&results{port: 22, authCount: 1}, assert.NoError,
		},
		{"key file",
			args{c: &Config{Port: 22, KeyFiles: []K{{kf(k2), ""}}, UseKeyFiles: true}},
			&results{port: 22, authCount: 1}, assert.NoError,
		},
		{"agent",
			args{c: &Config{Port: 22, UseAgent: true}, a: okAgent},
			&results{port: 22, authCount: 1, usedAgent: true}, assert.NoError,
		},
		{"no agent",
			args{c: &Config{Port: 22, UseAgent: true}},
			&results{usedAgent: true}, assert.Error,
		},
		{"agent error",
			args{c: &Config{Port: 22, UseAgent: true}, a: errAgent},
			&results{usedAgent: true}, assert.Error,
		},
		{"agent error",
			args{c: &Config{Port: 22, UseAgent: true}, a: errAgent},
			&results{usedAgent: true}, assert.Error,
		},
		{"agent error/ignore",
			args{c: &Config{Port: 22, UseAgent: true, IgnoreKeyErrors: true}, a: errAgent},
			&results{port: 22, usedAgent: true}, assert.NoError,
		},
		{"key error",
			args{c: &Config{Port: 22, Keys: []K{{"wrong key", pwd}}, UseKeys: true}},
			&results{}, assert.Error,
		},
		{"key error/ignored",
			args{c: &Config{Port: 22, Keys: []K{{"wrong key", pwd}}, UseKeys: true, IgnoreKeyErrors: true}},
			&results{port: 22}, assert.NoError,
		},
		{"key file error",
			args{c: &Config{Port: 22, KeyFiles: []K{{"wrong key", pwd}}, UseKeyFiles: true}},
			&results{}, assert.Error,
		},
		{"key file error/ignore",
			args{c: &Config{Port: 22, KeyFiles: []K{{"wrong key", pwd}}, UseKeyFiles: true, IgnoreKeyErrors: true}},
			&results{port: 22}, assert.NoError,
		},
		{"agent-key-key file error/ignore",
			args{c: &Config{Port: 22,
				UseAgent: true,
				Keys:     []K{{"wrong key", pwd}}, UseKeys: true,
				KeyFiles: []K{{"wrong key", pwd}}, UseKeyFiles: true, IgnoreKeyErrors: true}, a: errAgent},
			&results{usedAgent: true, port: 22}, assert.NoError,
		},
		{"host callback",
			args{c: &Config{Port: 22, HostKeyCallback: testCb}},
			&results{port: 22, cb: true}, assert.NoError,
		},
	}
	var res *results
	var actualAgent agent.Agent
	agent1New = func() (agent.Agent, net.Conn, error) {
		res.usedAgent = true
		var e error = nil
		if actualAgent == nil {
			e = errors.New("no agent")
		}
		return actualAgent, nil, e
	}
	sshDial = func(network, addr string, config *ssh.ClientConfig) (*ssh.Client, error) {
		re := `^(\w*):(\d+)$`
		r := regexp.MustCompile(re)
		found := r.FindAllStringSubmatch(addr, -1)
		if found != nil {
			res.host = found[0][1]
			var e error
			res.port, e = strconv.Atoi(found[0][2])
			if e != nil {
				res.wrongConn = true
			}
		} else {
			res.wrongConn = true
		}
		res.authCount = len(config.Auth)
		fmt.Println(config.Auth)
		res.user = config.User
		res.cb = errors.Is(config.HostKeyCallback("", nil, nil), fs.ErrInvalid)
		return nil, nil
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res = &results{}
			actualAgent = tt.args.a
			_, err := Connect(tt.args.c)
			if !tt.wantErr(t, err, fmt.Sprintf("Connect(%v)", tt.args.c)) {
				return
			}
			assert.Equalf(t, tt.want, res, "Connect(%v)", tt.args.c)
		})
	}
}

func Test_parseKeys(t *testing.T) {
	type args struct {
		keys         []K
		ignoreErrors bool
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{"simple/k2", args{[]K{{kd(t, k2), ""}}, false}, 1, assert.NoError},
		{"simple/k2p", args{[]K{{kd(t, k2p), pwd}}, false}, 1, assert.NoError},
		{"simple/k4", args{[]K{{kd(t, k4), ""}}, false}, 1, assert.NoError},
		{"simple/k4p", args{[]K{{kd(t, k4p), pwd}}, false}, 1, assert.NoError},
		{"simple/k2/pwd", args{[]K{{kd(t, k2), pwd}}, false}, 0, assert.Error},
		{"simple/k2p/pwd", args{[]K{{kd(t, k2p), ""}}, false}, 0, assert.Error},
		{"simple/k4/pwd", args{[]K{{kd(t, k4), pwd}}, false}, 0, assert.Error},
		{"simple/k4p/pwd", args{[]K{{kd(t, k4p), ""}}, false}, 0, assert.Error},
		{"simple/k2p/wrong pwd", args{[]K{{kd(t, k2p), "pwd1"}}, false}, 0, assert.Error},
		{"simple/k4p/wrong pwd", args{[]K{{kd(t, k4p), "pwd1"}}, false}, 0, assert.Error},
		{"complex/k2+k4p+k4", args{[]K{{kd(t, k2), ""}, {kd(t, k4p), pwd}, {kd(t, k4), ""}}, false}, 3, assert.NoError},
		{"complex/k2+k4p+k4/pwd", args{[]K{{kd(t, k2), ""}, {kd(t, k4p), pwd}, {kd(t, k4), pwd}}, false}, 0, assert.Error},
		{"complex/k2+k4p+k4/pwd/ignore", args{[]K{{kd(t, k2), ""}, {kd(t, k4p), pwd}, {kd(t, k4), pwd}}, true}, 2, assert.NoError},
		{"complex/k2+k4p+wrong", args{[]K{{kd(t, k2), ""}, {kd(t, k4p), pwd}, {"wrong key", pwd}}, false}, 0, assert.Error},
		{"complex/k2+k4p+wrong/ignore", args{[]K{{kd(t, k2), ""}, {kd(t, k4p), pwd}, {"wrong key", pwd}}, true}, 2, assert.NoError},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseKeys(tt.args.keys, tt.args.ignoreErrors)
			if !tt.wantErr(t, err, fmt.Sprintf("parseKeys(%v, %v)", tt.args.keys, tt.args.ignoreErrors)) {
				return
			}
			assert.Equalf(t, tt.want, len(got), "parseKeys(%v, %v)", tt.args.keys, tt.args.ignoreErrors)
		})
	}
}

func Test_parseKeyFiles(t *testing.T) {
	type args struct {
		keys         []K
		ignoreErrors bool
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{"simple/k2", args{[]K{{kf(k2), ""}}, false}, 1, assert.NoError},
		{"simple/k2p", args{[]K{{kf(k2p), pwd}}, false}, 1, assert.NoError},
		{"simple/k4", args{[]K{{kf(k4), ""}}, false}, 1, assert.NoError},
		{"simple/k4p", args{[]K{{kf(k4p), pwd}}, false}, 1, assert.NoError},
		{"simple/k2/pwd", args{[]K{{kf(k2), pwd}}, false}, 0, assert.Error},
		{"simple/k2p/pwd", args{[]K{{kf(k2p), ""}}, false}, 0, assert.Error},
		{"simple/k4/pwd", args{[]K{{kf(k4), pwd}}, false}, 0, assert.Error},
		{"simple/k4p/pwd", args{[]K{{kf(k4p), ""}}, false}, 0, assert.Error},
		{"simple/k2p/wrong pwd", args{[]K{{kf(k2p), "pwd1"}}, false}, 0, assert.Error},
		{"simple/k4p/wrong pwd", args{[]K{{kf(k4p), "pwd1"}}, false}, 0, assert.Error},
		{"complex/k2+k4p+k4", args{[]K{{kf(k2), ""}, {kf(k4p), pwd}, {kf(k4), ""}}, false}, 3, assert.NoError},
		{"complex/k2+k4p+k4/pwd", args{[]K{{kf(k2), ""}, {kf(k4p), pwd}, {kf(k4), pwd}}, false}, 0, assert.Error},
		{"complex/k2+k4p+k4/pwd/ignore", args{[]K{{kf(k2), ""}, {kf(k4p), pwd}, {kf(k4), pwd}}, true}, 2, assert.NoError},
		{"complex/k2+k4p+wrong", args{[]K{{kf(k2), ""}, {kf(k4p), pwd}, {kf(""), pwd}}, false}, 0, assert.Error},
		{"complex/k2+k4p+wrong/ignore", args{[]K{{kf(k2), ""}, {kf(k4p), pwd}, {kf(""), pwd}}, true}, 2, assert.NoError},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseKeyFiles(tt.args.keys, tt.args.ignoreErrors)
			if !tt.wantErr(t, err, fmt.Sprintf("parseKeyFiles(%v, %v)", tt.args.keys, tt.args.ignoreErrors)) {
				return
			}
			assert.Equalf(t, tt.want, len(got), "parseKeyFiles(%v, %v)", tt.args.keys, tt.args.ignoreErrors)
		})
	}
}

func kf(key string) string {
	return path.Join("test-keys", key)
}

func kd(t *testing.T, key string) string {
	keyData, e := ioutil.ReadFile(kf(key))
	assert.Nil(t, e)
	return string(keyData)
}

type testAgent struct {
	k ssh.Signer
	e error
}

func newAgent(k ssh.Signer, e error) *testAgent {
	return &testAgent{k: k, e: e}
}

func (t *testAgent) List() ([]*agent.Key, error) {
	return nil, errors.New("test")
}

func (t *testAgent) Sign(ssh.PublicKey, []byte) (*ssh.Signature, error) {
	return nil, errors.New("test")
}

func (t *testAgent) Add(agent.AddedKey) error {
	return errors.New("test")
}

func (t *testAgent) Remove(ssh.PublicKey) error {
	return errors.New("test")
}

func (t *testAgent) RemoveAll() error {
	return errors.New("test")
}

func (t *testAgent) Lock([]byte) error {
	return errors.New("test")
}

func (t *testAgent) Unlock([]byte) error {
	return errors.New("test")
}

func (t *testAgent) Signers() ([]ssh.Signer, error) {
	if t.k != nil {
		return []ssh.Signer{t.k}, t.e
	}
	return nil, t.e
}

var testKey = `-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACAKjLbokphrSwZ8zCqVBsGgX0CdX6tpR1oxojPn535MdAAAAJi56VgTuelY
EwAAAAtzc2gtZWQyNTUxOQAAACAKjLbokphrSwZ8zCqVBsGgX0CdX6tpR1oxojPn535MdA
AAAEAQFeNS28euAo6MEZCKwUDZUkbGeWIixGKn85ozHcb6EwqMtuiSmGtLBnzMKpUGwaBf
QJ1fq2lHWjGiM+fnfkx0AAAAEnJvbWFuQGJveGxldC5sb2NhbAECAw==
-----END OPENSSH PRIVATE KEY-----`

func testCb(string, net.Addr, ssh.PublicKey) error {
	return fs.ErrInvalid
}
