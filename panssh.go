/*
 * Copyright © 2022 Roman Pedchenko
 * Licensed under the MIT License that can be found in the LICENSE file.
 */

package panssh

import (
	"fmt"
	agent1 "github.com/xanzy/ssh-agent"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
)

type Config struct {
	Host            string
	Port            int
	User            string
	Pwd             string
	Keys            []K
	KeyFiles        []K
	HostKeyCallback ssh.HostKeyCallback
	UsePwd          bool
	UseKeys         bool
	UseKeyFiles     bool
	UseAgent        bool
	IgnoreKeyErrors bool
}

type K struct {
	K string
	P string
}

func Connect(c *Config) (*ssh.Client, error) {
	var signers []ssh.Signer
	if c.UseAgent {
		signers1, e := getAgentSigners()
		if e != nil && !c.IgnoreKeyErrors {
			return nil, e
		}
		signers = append(signers, signers1...)
	}
	if c.UseKeys {
		signers1, e := parseKeys(c.Keys, c.IgnoreKeyErrors)
		if e != nil {
			return nil, e
		}
		signers = append(signers, signers1...)
	}
	if c.UseKeyFiles {
		signers1, e := parseKeyFiles(c.KeyFiles, c.IgnoreKeyErrors)
		if e != nil {
			return nil, e
		}
		signers = append(signers, signers1...)
	}
	var auth []ssh.AuthMethod
	if len(signers) > 0 {
		auth = append(auth, ssh.PublicKeys(signers...))
	}
	if c.UsePwd {
		auth = append(auth, ssh.Password(c.Pwd))
	}
	var hostKeyCallback ssh.HostKeyCallback
	if c.HostKeyCallback != nil {
		hostKeyCallback = c.HostKeyCallback
	} else {
		hostKeyCallback = ssh.InsecureIgnoreHostKey()
	}
	conn, e := sshDial("tcp", fmt.Sprintf("%s:%d", c.Host, c.Port), &ssh.ClientConfig{
		User:            c.User,
		HostKeyCallback: hostKeyCallback,
		Auth:            auth,
	})
	return conn, e
}

var agent1New = agent1.New
var sshDial = ssh.Dial

func getAgentSigners() ([]ssh.Signer, error) {
	agnt, _, e := agent1New()
	if e != nil {
		return nil, e
	}
	return agnt.Signers()
}

func parseKeys(keys []K, ignoreErrors bool) ([]ssh.Signer, error) {
	var signers []ssh.Signer
	for _, k := range keys {
		signer, e := parseKey([]byte(k.K), k.P)
		if e != nil {
			if ignoreErrors {
				continue
			}
			return nil, e
		}
		signers = append(signers, signer)
	}
	return signers, nil
}

func parseKeyFiles(keys []K, ignoreErrors bool) ([]ssh.Signer, error) {
	var signers []ssh.Signer
	for _, k := range keys {
		keyData, e := ioutil.ReadFile(k.K)
		if e != nil {
			if ignoreErrors {
				continue
			}
			return nil, fmt.Errorf("error reading key file: %v", e)
		}
		signer, e := parseKey(keyData, k.P)
		if e != nil {
			if ignoreErrors {
				continue
			}
			return nil, e
		}
		signers = append(signers, signer)
	}
	return signers, nil
}

func parseKey(keyData []byte, pwd string) (signer ssh.Signer, e error) {
	if pwd != "" {
		signer, e = ssh.ParsePrivateKeyWithPassphrase(keyData, []byte(pwd))
	} else {
		signer, e = ssh.ParsePrivateKey(keyData)
	}
	return signer, e
}
