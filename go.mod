module gitlab.com/elfuego/panssh

go 1.17

require (
	github.com/stretchr/testify v1.6.1
	github.com/xanzy/ssh-agent v0.3.1
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
)

require (
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
